from joblib import dump
import pandas as pd
import dask.dataframe as dd
import h2o
from mlinsights.mlmodel import QuantileLinearRegression
import numpy as np
import sys
from TOOLS import GetFoldByAsset_pd

d1 = int(sys.argv[1])
d2 = int(sys.argv[2])

h2o.init()
data = h2o.import_file("preprocess/training_wide_mean_ret_interpolated_vol", pattern=".*.part")
data_pd = data.as_data_frame()

for prod_id in range(d1,d2+1):
    print(prod_id)
    train, valid = GetFoldByAsset_pd(prod_id, data_pd)

    features_basic = [col for col in train.columns if "aggreg" in col]
    features = [col for col in features_basic if   col.endswith(f"_{prod_id}_")]

    QuantileReg = QuantileLinearRegression(max_iter=250)

    train["aggreg_daily"] = train[features_basic].replace(0, np.nan).mean(axis=1)

    features.append("aggreg_daily")

    QuantileReg.fit(train[features],
                    train[f"TARGET_{prod_id}_"],
                    sample_weight=train[f"WEIGHT_GBM_{prod_id}_"])

    dump(QuantileReg,f"generated_models/asset_by_asset/quantile_regression_{prod_id}")
