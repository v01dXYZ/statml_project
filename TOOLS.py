import pandas as pd
import numpy as np
import h2o
from h2o.estimators.glm import H2OGeneralizedLinearEstimator


def mape(pred, target):
    if isinstance(pred, h2o.H2OFrame):
        mape_score = ((max(0,pred) - target) / target).abs().mean()
        return 100*mape_score[0]
    else:
        mape_score = np.abs((pred.clip(0) - target) / target).mean()
        return 100*mape_score


def Get_best_model_wide(list_models, valid, prod_id):
    scores = []
    for i in range(0,len(list_models)):
        scores.append(mape(max(0,list_models[i].predict(valid)),valid[f"TARGET_{prod_id}_"]))

    return list_models[scores.index(min(scores))]    



def Search_best_model_lambda_wide(model,train,valid,prod_id):
    path = pd.DataFrame(
            H2OGeneralizedLinearEstimator.getGLMRegularizationPath(model))

    path["models"] = path["coefficients"].apply(
            lambda c: H2OGeneralizedLinearEstimator.makeGLMModel(model, c))
    path["score"] = path["models"].apply(lambda m: mape(
            max(0,m.predict(valid)), valid[f"TARGET_{prod_id}_"]))

    best_model = path.loc[path["score"].idxmin(), "models"]

    return best_model

def Search_best_model_lambda(model,train,valid):
    path = pd.DataFrame(
            H2OGeneralizedLinearEstimator.getGLMRegularizationPath(model))

    path["models"] = path["coefficients"].apply(
            lambda c: H2OGeneralizedLinearEstimator.makeGLMModel(model, c))
    path["score"] = path["models"].apply(lambda m: mape(
            max(0,m.predict(valid)), valid[TARGET]))

    best_model = path.loc[path["score"].idxmin(), "models"]

    return best_model

def Get_best_model(list_models, valid):
    scores = []
    for i in range(0,len(list_models)):
        scores.append(mape(max(0,list_models[i].predict(valid)),valid["TARGET"]))

    return list_models[scores.index(min(scores))]    



def Search_best_model_lambda(model,train,valid):
    path = pd.DataFrame(
            H2OGeneralizedLinearEstimator.getGLMRegularizationPath(model))

    path["models"] = path["coefficients"].apply(
            lambda c: H2OGeneralizedLinearEstimator.makeGLMModel(model, c))
    path["score"] = path["models"].apply(lambda m: mape(
            max(0,m.predict(valid)), valid["TARGET"]))

    best_model = path.loc[path["score"].idxmin(), "models"]

    return best_model


def GetFoldByAsset_h2o(product_id, data_h2o, features=None):
    dumb_frame = h2o.H2OFrame(np.arange(2122))
    runif_sequence = dumb_frame.runif(seed=42)
    is_in_validation = (runif_sequence > 0.8)

    mask = (data_h2o[f"TARGET_{product_id}_"] != 0)
    data = data_h2o[mask]
    nrow = int(mask.sum())
    tmp_is_in_validation = is_in_validation[:nrow,:]

    validation = data[tmp_is_in_validation,:]
    train = data[~tmp_is_in_validation, :]

    return train, validation


def GetFoldByAsset_pd(product_id, data_pd):
    dumb_frame = h2o.H2OFrame(np.arange(2122))
    runif_sequence = dumb_frame.runif(seed=42).as_data_frame()
    is_in_validation = (runif_sequence > 0.8)
    h2o.remove(dumb_frame)

    data = data_pd.loc[
        data_pd[f"TARGET_{product_id}_"].ne(0)
    ]
    
    nrow = data.shape[0]
    tmp_is_in_validation = is_in_validation.iloc[:nrow]

    validation = data[tmp_is_in_validation.values]
    train = data[~tmp_is_in_validation.values]

    return train, validation
