from abc import ABC, abstractmethod

from datetime import datetime
from pathlib import Path

import pandas as pd
import h2o

from preprocess import preprocess_folder
from submit import SubmitResult

generated_models_folder = Path("models")
generated_predictions_folder = Path("predictions")


def mape(pred, target):
    mape_score = ((pred - target) / target).abs().mean()

    return mape_score[0]


class CFM(ABC):
    def __init__(self):
        self.train_data_folder = preprocess_folder / self.train_data_name
        self.test_data_folder = preprocess_folder / self.test_data_name

        if not self.train_data_folder.exists():
            raise Exception(f"No data folder named {self.train_data_folder}")
        elif not self.train_data_folder.is_dir():
            raise Exception(f"{self.train_data_folder} is not a directory")

        train_data_preview_header_part_list = [
            f for f in self.train_data_folder.glob("*.part") if set(f.name[:-5]) == {"0"}
        ]

        if not train_data_preview_header_part_list:
            raise Exception(f"No header file in {self.train_data_folder}")
        elif len(train_data_preview_header_part_list) >= 2:
            raise Exception(f"Too many header files in {self.train_data_folder}")

        train_data_preview_header_part = train_data_preview_header_part_list[0]

        train_data_preview = pd.read_csv(train_data_preview_header_part, nrows=1)

        features_keywords = ["volatility", "return"]

        self.features = [
            col
            for col in train_data_preview.columns
            if any(kw in col for kw in features_keywords)
        ]

        return_cols = [col for col in train_data_preview.columns if "return" in col]
        col_types = {return_col: "enum" for return_col in return_cols}

        self.train_data = h2o.import_file(
            str(self.train_data_folder), pattern=".*.part", col_types=col_types
        )

        create_validation_data = getattr(self, "create_validation_data", None)
        if create_validation_data is not None:
            if isinstance(create_validation_data, int):
                seed = int(create_validation_data)
                self.train_data, self.validation_data = self.train_data.split_frame(
                    ratios=[0.8], seed=seed
                )
            else:
                self.train_data, self.validation_data = self.train_data.split_frame(
                    ratios=[0.8]
                )

    def submit(self):
        predictions = self.test_predictions()

        current_time = datetime.now().strftime("%Y-%m-%d_%H_%m")

        generated_predictions_folder.mkdir(exist_ok=True)

        predictions_path = (
            generated_predictions_folder / f"{self.name}_{current_time}.csv"
        )
        predictions.to_csv(predictions_path)

        predictions_file = predictions_path.open()
        submission = SubmitResult(predictions_file)

        score = submission.submit()

        print("Score:", score)

    def test_predictions(self):
        test = h2o.import_file(str(self.test_data_folder), pattern=".*.part")

        predictions = self.predict(test)

        return predictions

    def train_predictions(self):
        predictions = self.predict(self.train_data)
        training_y = pd.read_csv(
            "data/challenge_34_cfm_trainingoutputfile.csv", index_col="ID"
        )

        predictions_with_residuals = pd.DataFrame(
            {
                "prediction": predictions,
                "target": training_y,
                "residuals": training_y - predictions,
            }
        )

        return predictions_with_residuals


    @abstractmethod
    def predict(self, df):
        pass

    @abstractmethod
    def train(self):
        pass


class LongFormatModel(CFM):
    def train(self):
        model = self.create_model()

        model_folder = generated_models_folder / self.name
        model_folder.mkdir(exist_ok=True, parents=True)

        model.download_mojo(str(model_folder / "all_products"))

    def predict(self, df):
        model = h2o.upload_mojo(str(generated_models_folder / self.name / "all_products"))

        raw_predictions_h2o = model.predict(df)
        raw_predictions = raw_predictions_h2o.as_data_frame()["predict"]

        h2o.remove(raw_predictions_h2o)

        predictions = pd.DataFrame(
            {"TARGET": raw_predictions.values},
            index=df["ID"].as_data_frame().values.flatten(),
        )

        sorted_predictions = predictions.loc[
            predictions.index[
                (predictions.index != 0) & predictions.index.notnull()
            ].sort_values()
        ]

        return sorted_predictions

    @abstractmethod
    def create_model():
        pass


class WideFormatModel(CFM):
    train_data_name = "training_wide"
    test_data_name = "testing_X_wide"

    def train(self):
        for product_id in range(1, 319):
            model = self.create_model(product_id)

            predictions = model.predict(self.train_data)
            train_score = mape(predictions, self.train_data[f"TARGET_{product_id}"])
            h2o.remove(predictions)

            print("Train score", train_score)

            wide_glm_folder = generated_models_folder / self.name
            wide_glm_folder.mkdir(exist_ok=True, parents=True)

            model_saving_path = str(wide_glm_folder / f"{product_id}")
            model.download_mojo(model_saving_path)

            # Remove the model to avoid hitting memory ceiling
            h2o.remove(model)

    def predict_per_product(self, df, product_id):
        model = h2o.upload_mojo(
            str(generated_models_folder / self.name / f"{product_id}")
        )

        raw_predictions_h2o = model.predict(df)
        raw_predictions = raw_predictions_h2o.as_data_frame()["predict"]

        h2o.remove(raw_predictions_h2o)

        predictions = pd.DataFrame(
            {"TARGET": raw_predictions.values},
            index=(df[f"ID_{product_id}"].as_data_frame().values.flatten()),
        )

        h2o.remove([model.model_key, model])

        return predictions

    def predict(self, df):
        predictions = pd.concat(
            [self.predict_per_product(df, product_id) for product_id in range(1, 319)]
        )

        sorted_predictions = predictions.loc[
            predictions.index[
                (predictions.index != 0) & predictions.index.notnull()
            ].sort_values()
        ]

        return sorted_predictions

    @abstractmethod
    def create_model(self, product_id):
        pass
