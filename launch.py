import argparse

import h2o

from main import (
    BasicGradientBoosting,
    BasicRandomForest,
    WideFormatLasso,
    WideFormatRidge,
    LongFormatLasso,
    WideFormatConstrainedRidge,
)


name_to_model = {
    "gbm": BasicGradientBoosting,
    "rf": BasicRandomForest,
    "w_lasso": WideFormatLasso,
    "w_ridge": WideFormatRidge,
    "l_lasso": LongFormatLasso,
    "w_c_ridge": WideFormatConstrainedRidge,
}

parser = argparse.ArgumentParser(
    description="Launch models", formatter_class=argparse.RawTextHelpFormatter
)
parser.add_argument(
    "model",
    choices=name_to_model.keys(),
    help=(
        "Model name"
        + "".join([f"\n- {cls.__name__}: {name}" for name, cls in name_to_model.items()])
    ),
)

parser.add_argument("--submit", action="store_true", help="Submit only")

args = parser.parse_args()

model_name = args.model
submit_only = args.submit

h2o.init(min_mem_size="16G")

model_class = name_to_model[model_name]
model = model_class()

if not submit_only:
    model.train()

model.submit()
