from pathlib import Path

import joblib
import h2o

import pandas as pd
import numpy as np

from tqdm import tqdm

result_dir = Path("results")
result_dir.mkdir(exist_ok=True)

h2o.init()

data_h2o = h2o.import_file("preprocess/testing_X_wide_mean_ret_interpolated_vol", pattern=".*.part")
data = data_h2o.as_data_frame()

predictions_list = []

for prod_id in tqdm(range(1, 319)):
    features_basic = [col for col in data.columns if "aggreg" in col]

    data["aggreg_daily"] = data[features_basic].replace(0, np.nan).mean(axis=1)
    
    features = [col for col in features_basic if   col.endswith(f"_{prod_id}_")]
    features.append("aggreg_daily")

    QuantileReg = joblib.load(f"generated_models/asset_by_asset/quantile_regression_{prod_id}")

    predictions_raw = QuantileReg.predict(data[features])

    predictions_with_null_id = pd.DataFrame({
        "ID": data[f"ID_{prod_id}_"].values,
        "TARGET": predictions_raw.clip(0),
    })

    predictions = predictions_with_null_id.query("ID != 0")

    predictions_list.append(predictions)

predictions_all_product = pd.concat(predictions_list,
                                    ignore_index=False)

(predictions_all_product
 .sort_values("ID")
 .to_csv(
     "results/test_predictions_quantile_asset_by_asset.csv",
     index=False))
