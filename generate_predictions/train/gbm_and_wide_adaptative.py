from pathlib import Path

import h2o

from tqdm import tqdm

result_dir = Path("results")
result_dir.mkdir(exist_ok=True)

h2o.init()

print("Loading train set with H2O")
training_wide_mean_ret_interpolated_vol = h2o.import_file(
    "preprocess/training_wide_mean_ret_interpolated_vol/", pattern=".*.part")

model_fmt_str = {
    "gbm":
    "generated_models/asset_by_asset/gbm_vol_aggreg_asset_{product_id}",
    "wide_adaptative":
    "generated_models/wide_asset_by_asset/wide_adaptative_GLM_vol_aggreg_{product_id}"
}

h2o.no_progress()

predictions_lists = {"gbm": [], "wide_adaptative": []}

for product_id in tqdm(range(1, 319)):
    for model_name in ["gbm", "wide_adaptative"]:
        model = h2o.load_model(
            model_fmt_str[model_name].format(product_id=product_id))

        predictions_with_null_id = (
            training_wide_mean_ret_interpolated_vol[[
                f"ID_{product_id}_", f"TARGET_{product_id}_"
            ]].cbind(max(0, model.predict(training_wide_mean_ret_interpolated_vol))))

        predictions_with_null_id["product_id"] = product_id
        predictions_with_null_id.columns = ["ID", "TARGET","predict", "product_id"]

        predictions_with_null_id.refresh()

        predictions_lists[model_name].append(predictions_with_null_id)

h2o.show_progress()

for model_name in ["gbm", "wide_adaptative"]:
    predictions_list = predictions_lists[model_name]

    predictions_all_product = predictions_list[0].rbind(predictions_list[1:])
    predictions_all_product = predictions_all_product[
        predictions_all_product["ID"] != 0, :]

    h2o.export_file(predictions_all_product.sort("ID"),
                    f"results/predictions_{model_name}_asset_by_asset.csv",
                    force=True)
