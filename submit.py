import io
import sys

import re
import requests

import toml


class SubmitResult:
    csrfmiddlewaretoken_re = re.compile("name='csrfmiddlewaretoken' value='(\w*)")
    score_re = re.compile("score is : ([0-9\\.]+|nan)")

    challenge_data_url = "https://challengedata.ens.fr"

    login_url = f"{challenge_data_url}/login/"
    submit_url = f"{challenge_data_url}/participants/challenges/22/submit"

    session_headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0"
    }

    tokens = {"csrfmiddlewaretoken": None}

    def __init__(self, file):
        self.file = file

    def crfmiddlewaretoken(f):
        def wrap(self, session):
            request = f(self, session)

            # Django supports two anti-csrf tokens by cooky and by post
            self.tokens["csrfmiddlewaretoken"] = session.cookies["csrftoken"]
            # get_csrfmiddlewaretoken_from_request(request)

            return request

        return wrap

    @crfmiddlewaretoken
    def get_login_token(self, session):
        prelogin_response = session.get(self.submit_url)

        return prelogin_response

    @crfmiddlewaretoken
    def cfm_login(self, session):

        credentials = toml.load("cfm_credentials.toml")

        self.get_login_token(session)

        login_response = session.post(
            self.login_url,
            params={"next": "/participants/challenges/22/submit"},
            data={
                "username": credentials["username"],
                "password": credentials["password"],
                **self.tokens,
            },
        )

        if "LOGOUT" not in login_response.text:
            raise Exception("Login Failed. Are you sure the credentials are right ?")

        return login_response

    def submit_solution(self, session, solution):
        submit_solution_answer = session.post(
            self.submit_url,
            data={"method": "hello", "parameters": "hello", **self.tokens},
            files={"y_test_csv_file": solution},
        )

        return submit_solution_answer

    def submit(self):
        with requests.Session() as session:
            session.headers.update(self.session_headers)

            self.cfm_login(session)

            submit_solution_request = self.submit_solution(session, self.file)

            score = self.get_score_from_request(submit_solution_request)

            return score

    def get_score_from_request(self, request):
        score_search = self.score_re.search(request.text)

        if score_search is None:

            open("failed_submission.html", "w+").write(request.text)

            raise Exception(
                "The submission failed. Please look at failed_submission.html to debug"
            )

        score_str = score_search.group(1)

        score = float(score_str)

        rounded_score = round(score, 4)

        return rounded_score


def submit(filepath):
    s = SubmitResult(open(filepath))
    score = s.submit()

    return score

def submit_dataframe(df):
    data = io.StringIO(df.to_csv(index=False))
    
    s = SubmitResult(data)
    score = s.submit()

    return score
    


if __name__ == "__main__":
    filepath = sys.argv[1]

    score = submit(filepath)
    print(f'Score for submitted file "{filepath}"', score)
