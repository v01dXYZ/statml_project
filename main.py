from h2o.estimators.glm import H2OGeneralizedLinearEstimator
from h2o.estimators.random_forest import H2ORandomForestEstimator
from h2o.estimators.gbm import H2OGradientBoostingEstimator
from h2o.estimators.stackedensemble import H2OStackedEnsembleEstimator

from models import LongFormatModel, WideFormatModel

import pandas as pd
from models import mape


class BasicGradientBoosting(LongFormatModel):
    name = "basic_gradient_boosting"
    train_data_name = "training_basic_imputation"
    test_data_name = "testing_X_basic_imputation"

    def __init__(self, nfolds=5, ntrees=50, max_depth=8):
        LongFormatModel.__init__(self)

        self.nfolds = nfolds
        self.ntrees = ntrees
        self.max_depth = max_depth

    def create_model(self):
        GradientBoosting = H2OGradientBoostingEstimator(
            nfolds=self.nfolds,
            ntrees=self.ntrees,
            max_depth=self.max_depth,
            min_rows=1,
            distribution="huber",
            huber_alpha=0,
            weights_column="WEIGHT",
        )

        GradientBoosting.train(self.features,
                               "TARGET",
                               training_frame=self.train_data)

        return GradientBoosting


class BasicRandomForest(LongFormatModel):
    name = "basic_random_forest"
    train_data_name = "training_without_imputation"
    test_data_name = "testing_X_without_imputation"

    def __init__(self, ntrees=100, max_depth=5):
        LongFormatModel.__init__(self)

        self.ntrees = ntrees
        self.max_depth = max_depth

    def create_model(self):
        model = H2ORandomForestEstimator(
            model_id="basic_random_forest",
            ntrees=self.ntrees,
            max_depth=self.max_depth,
            weights_column="WEIGHT",
            min_rows=1,
        )

        model.train(self.features, "TARGET", training_frame=self.train_data)

        return model


def get_best_lambdas(create_model):
    def wrapped(self, *args):
        model = create_model(self, *args)

        path = pd.DataFrame(
            H2OGeneralizedLinearEstimator.getGLMRegularizationPath(model))

        path["models"] = path["coefficients"].apply(
            lambda c: H2OGeneralizedLinearEstimator.makeGLMModel(model, c))
        path["validation_score"] = path["models"].apply(lambda m: mape(
            m.predict(self.validation_data), self.validation_data["TARGET"]))

        best_model = path.loc[path["validation_score"].idxmin(), "models"]

        return best_model

    return wrapped


class WideFormatRidge(WideFormatModel):
    name = "wide_ridge"

    def create_model(self, product_id):
        ridge_reg = H2OGeneralizedLinearEstimator(
            alpha=0,
            lambda_search=True,
            fold_column="fold_column",
            keep_cross_validation_models=True,
            keep_cross_validation_predictions=True,
            early_stopping=False,
            family="gaussian",
        )

        ridge_reg.train(
            self.features,
            f"TARGET_{product_id}",
            training_frame=self.train_data,
            weights_column=f"WEIGHT_{product_id}",
        )

        return ridge_reg


class WideFormatLasso(WideFormatModel):
    name = "wide_lasso"

    def create_model(self, product_id):
        lasso_reg = H2OGeneralizedLinearEstimator(
            alpha=1,
            lambda_search=True,
            fold_column="fold_column",
            keep_cross_validation_models=True,
            keep_cross_validation_predictions=True,
            early_stopping=False,
            family="gaussian",
        )

        lasso_reg.train(
            self.features,
            f"TARGET_{product_id}",
            training_frame=self.train_data,
            weights_column=f"WEIGHT_{product_id}",
        )

        return lasso_reg


class LongFormatLasso(LongFormatModel):

    name = "long_lasso"
    train_data_name = "training_basic_imputation"
    test_data_name = "testing_X_basic_imputation"
    create_validation_data = True

    @get_best_lambdas
    def create_model(self):

        lasso_reg = H2OGeneralizedLinearEstimator(
            alpha=1,
            lambda_search=True,
            lambda_min_ratio=0.0001,
            nlambdas=500,
            keep_cross_validation_models=True,
            keep_cross_validation_predictions=True,
            early_stopping=False,
            seed=42,
            family="gaussian",
        )

        lasso_reg.train(
            self.features,
            f"TARGET",
            training_frame=self.train_data,
            validation_frame=self.validation_data,
            weights_column=f"WEIGHT",
        )

        return lasso_reg


class WideFormatConstrainedRidge(WideFormatModel):

    name = "wide_format_constrained_ridge"
    train_data_name = "training_wide_mean_ret_interpolated_vol"
    test_data_name = "testing_X_wide_mean_ret_interpolated_vol"
    create_validation_data = True

    def create_model(self, product_id):
        lasso_reg = H2OGeneralizedLinearEstimator(alpha=0,
                                                  lambda_search=True,
                                                  family="gaussian")

        only_product_features = [
            feature for feature in self.features
            if feature.endswith(f"_{product_id}")
        ]

        lasso_reg.train(
            only_product_features,
            f"TARGET_{product_id}",
            training_frame=self.train_data,
            validation_frame=self.validation_data,
            weights_column=f"WEIGHT_{product_id}",
        )

        return lasso_reg
