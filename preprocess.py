import logging
from pathlib import Path

import numpy as np

import pandas as pd

import dask
import dask.dataframe as dd

from dask.distributed import LocalCluster, Client, progress

preprocess_folder = Path("preprocess")


def preprocess():

    logging.info("[1/4] Loading Raw Data")
    training_X, training_y, testing_X = load_raw_data()
    training = long_format_from_raw(training_X, training_y)

    logging.info(
        "[2/4] Imputing Long Format Data wih Basic Rule\n"
        "      Keep only average returns and Interpolate Volatilites"
    )
    training_basic_imputation = long_format_basic_imputation(training.copy())
    testing_X_basic_imputation = long_format_basic_imputation(testing_X.copy())

    training_mean_ret_interpolated_vol = long_format_mean_ret_interpolate_vol(training.copy())
    testing_X_mean_ret_interpolated_vol = long_format_mean_ret_interpolate_vol(testing_X.copy())

    logging.info("[3/4] Creating Wide Format Data")

    logging.info("    (1/4) training basic imputation")
    training_wide = wide_from_long(training_basic_imputation)

    logging.info("    (2/4) testing basic imputation")
    testing_X_wide = wide_from_long(testing_X_basic_imputation)

    logging.info("    (3/4) training mean ret only, interpolated vol")

    training_wide_mean_ret_interpolated_vol = wide_from_long(
        training_mean_ret_interpolated_vol
    )

    logging.info("    (4/4) testing mean ret only, interpolated vol")
    testing_X_wide_mean_ret_interpolated_vol = wide_from_long(
        testing_X_mean_ret_interpolated_vol
    )

    logging.info("[4/4] Saving to Disk")
    save_preprocess_data(
        {
            "training_without_imputation": training,
            "testing_X_without_imputation": testing_X,
            "training_basic_imputation": training_basic_imputation,
            "testing_X_basic_imputation": testing_X_basic_imputation,
            "training_mean_ret_interpolated_vol" : training_mean_ret_interpolated_vol,
            "testing_X_mean_ret_interpolated_vol" : testing_X_mean_ret_interpolated_vol,
            "training_wide": training_wide,
            "testing_X_wide": testing_X_wide,
            "training_wide_mean_ret_interpolated_vol": training_wide_mean_ret_interpolated_vol,
            "testing_X_wide_mean_ret_interpolated_vol": testing_X_wide_mean_ret_interpolated_vol,
        }
    )


def load_raw_data():
    training_X = dd.read_csv("data/training_input.csv", sep=";").categorize(
        columns=["date"]
    ).reset_index(drop=True)
    training_y = dd.read_csv("data/challenge_34_cfm_trainingoutputfile.csv")

    testing_X = dd.read_csv("data/testing_input.csv", sep=";").categorize(
        columns=["date"]
    ).reset_index(drop=True)

    return training_X, training_y, testing_X


def long_format_from_raw(training_X, training_y):
    training = training_X.merge(training_y, on="ID")
    training["WEIGHT_GBM"] = training["TARGET"].pow(-1)
    training["WEIGHT_GLM"] = training["TARGET"].pow(-1.5)

    return training


def long_format_basic_imputation(df):
    """
    Perform a last valid value or 0 for volatility features 
    and return features separately
    """

    for cols in get_ret_vol_cols(df):
        df[cols] = df[cols].ffill(axis=1).fillna(0)

    return df


def get_ret_vol_cols(df):
    ret_cols = [col for col in df.columns if "return" in col]
    vol_cols = [col for col in df.columns if "volatility" in col]

    return ret_cols, vol_cols


def long_format_mean_ret_interpolate_vol(df):
    ret_cols, vol_cols = get_ret_vol_cols(df)

    vol_cols = pd.Series(vol_cols)

    df = df.copy()  # Necessay since dropping data

    df["mean_ret"] = df[ret_cols].mean(axis=1)
    df = df.drop(axis=1,labels=ret_cols)

    df[vol_cols].map_partitions(
        lambda d: d.interpolate(axis=1, limit_directions="both", inplace=True)
    )

    def append_aggregateted_vols(d):
        vol_cols = d.columns[d.columns.str.contains("volatility")]
        aggregated_volatilities = (d[vol_cols]
                                   .groupby("vol_aggreg_" +
                                            vol_cols.str[11:13] +
                                            "_" +
                                            (vol_cols.str[14].astype(int)
                                             // 3).astype(str)
                                            ,
                                            axis=1)
                                   .mean())

        return d.assign(**aggregated_volatilities)

    df = df.map_partitions(append_aggregateted_vols)

    df["date"] = df["date"].astype(int)
    df["aggreg_daily"] = df["date"].map(df.groupby("date")[vol_cols].mean()).astype(float)

    return df


def wide_from_long(long_df):
    meta = long_df[[col for col in long_df.columns if col not in ["product_id", "date"]]]
    future_wide_df = long_df.groupby("date").apply(
        lambda d: (d.drop(columns=["date"]).set_index("product_id")), meta=meta
    )

    logging.info("        long to wide")
    future_wide_df = future_wide_df.persist()
    progress(future_wide_df)
    print()

    pd_wide_df = future_wide_df.compute().unstack().reset_index()

    logging.info("        cast/filla")
    pd_wide_df["date"] = pd_wide_df["date"].astype(int)

    pd_wide_df.fillna(0, inplace=True)

    columns_flatten_multiindex_to_index(pd_wide_df)

    wide_df = dd.from_pandas(pd_wide_df, npartitions=5)

    return wide_df


def columns_flatten_multiindex_to_index(df):
    """
    Normalizing column names for easier processing
    """

    df.columns = (
        df.columns.to_frame()
        .astype(str)
        .apply("_".join, axis=1)
        .str.strip()
        .str.replace(" ", "_")
        .str.replace("_$", "")
        + "_"
    )  # We remove the trailing underscore
    #    for "date"


def save_preprocess_data(name_to_frame):
    preprocess_folder.mkdir(exist_ok=True)

    delayed_to_csv = [
        categorical_to_int(df).to_csv(
            preprocess_folder / name,
            index=False,
            compute=False,
        )
        for name, df in name_to_frame.items()
    ]

    progress(dask.persist(*delayed_to_csv))


def categorical_to_int(df):
    """
    Necessary since the to_csv can raise an error
    if the partitions don't have the same categories
    for the categorical columns.
    """
    dtypes = df.dtypes

    cat_cols = dtypes.index[dtypes.eq("category")]

    return df.astype({col: int for col in cat_cols})


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    cluster = LocalCluster(threads_per_worker=1)
    client = Client(cluster)

    preprocess()
