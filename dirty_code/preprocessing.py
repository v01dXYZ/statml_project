from zipfile import ZipFile
from sklearn.model_selection import train_test_split
from list_delete_asset import listing
import pandas as pd


def vol_interpolate_return_basic(df):
    ret_cols = [col for col in df.columns if "return" in col]
    vol_cols = [col for col in df.columns if "volatility" in col]

    for cols in [ret_cols]:
        df[cols] = df[cols].fillna(0)

    df[vol_cols] = df[vol_cols].interpolate(axis=1, limit_directions="both")
    df[vol_cols] = df[vol_cols].fillna(0)
    df["mean_ret"] = df[ret_cols].mean(axis=1)
    df = df.drop(columns=ret_cols)

    return df


def columns_flatten_multiindex_to_index(df):
    """
    Normalizing column names for easier processing
    """
    df.columns = (
        df.columns.to_frame().astype(str).apply("_".join, axis=1).str.replace(" ", "_")
        + "_"
    )


#####################################################################


print("Loading Raw data")
print("    Train set:")

training_X_zip = ZipFile("/workdir/ait/projet/data/training_input.csv.zip")
training_X_file = training_X_zip.open("training_input.csv")
training_X = pd.read_csv(training_X_file, sep=";")


training_y = pd.read_csv(
    "/workdir/ait/projet/data/challenge_34_cfm_trainingoutputfile.csv"
)


print("    Test set:")

testing_X_zip = ZipFile("/workdir/ait/projet/data/testing_input.csv.zip")
testing_X_file = testing_X_zip.open("testing_input.csv")
testing_X = pd.read_csv(testing_X_file, sep=";")

print("Creating Long Format DataFrames")
training = training_X.merge(training_y, on="ID").astype(
    {"product_id": "category", "date": "category"}
)

training.to_csv("/workdir/ait/projet/data/training_no_modif.csv")


print("Features modifications (mean returns + volatility interpolation + weights)")
print("     Creating long format dataframes (train + valid) all asset")

training["WEIGHT_GLM"] = training["TARGET"].pow(-(3 / 2)).fillna(0)
training["WEIGHT_GBM"] = training["TARGET"].pow(-1).fillna(0)
training = vol_interpolate_return_basic(training)
train, valid = train_test_split(training, test_size=0.2)
training.to_csv("/workdir/ait/projet/data/long_format/training_all_asset.csv")
train.to_csv("/workdir/ait/projet/data/long_format/train_all_asset.csv")
valid.to_csv("/workdir/ait/projet/data/long_format/valid_all_asset.csv")

print("     Creating dataframes (train + valid) asset by asset")

for prod_id in range(1, 319):
    training_by_asset = training[training["product_id"] == prod_id]
    train, valid = train_test_split(training_by_asset, test_size=0.2)
    training_by_asset.to_csv(
        f"/workdir/ait/projet/data/long_format/training_asset_{prod_id}.csv"
    )
    train.to_csv(f"/workdir/ait/projet/data/long_format/train_asset_{prod_id}.csv")
    valid.to_csv(f"/workdir/ait/projet/data/long_format/valid_asset_{prod_id}.csv")


print("Creating Wide Format DataFrame")

per_date = training.pivot(index="date", columns="product_id")

columns_flatten_multiindex_to_index(per_date)

print("    Creating weights")
target_cols = [f"TARGET_{i}_" for i in range(1, 319)]
weight_cols_GLM = [f"WEIGHT_GLM_{i}_" for i in range(1, 319)]
weight_cols_GBM = [f"WEIGHT_GBM_{i}_" for i in range(1, 319)]

per_date[weight_cols_GLM] = per_date[target_cols].pow(-(3 / 2)).fillna(0)
per_date[weight_cols_GLM] = per_date[target_cols].pow(-(1)).fillna(0)

per_date.to_csv("/workdir/ait/projet/data/wide_format/per_date.csv", index=False)

print("     Creating wide formats dataframes bloc by bloc")

liste = listing(per_date)
print(liste)
for prod_id in range(1, 319):
    wide_training_by_asset = per_date[per_date[f"TARGET_{prod_id}_"] > 0]
    wide_training_by_asset = wide_training_by_asset.drop(
        columns=[
            col for col in per_date if any(item in col for item in liste[prod_id - 1])
        ]
    )
    wide_train, wide_valid = train_test_split(wide_training_by_asset, test_size=0.2)
    wide_training_by_asset.to_csv(
        f"/workdir/ait/projet/data/wide_format/wide_training_asset_{prod_id}.csv"
    )
    wide_train.to_csv(
        f"/workdir/ait/projet/data/wide_format/wide_train_asset_{prod_id}.csv"
    )
    wide_valid.to_csv(
        f"/workdir/ait/projet/data/wide_format/wide_valid_asset_{prod_id}.csv"
    )
