import pandas as pd


def listing(file):

    liste_assets = []

    for prod_id in range(1, 319):
        liste_assets.append([])
        delete = 0
        for i in range(1, 319):
            delete = 0
            if any(
                item == 0 for item in file[file[f"TARGET_{prod_id}_"] > 0][f"TARGET_{i}_"]
            ):
                delete = 1

        if delete == 1:
            liste_assets[prod_id - 1].append(i)

    return liste_assets
