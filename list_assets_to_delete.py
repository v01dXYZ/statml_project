"""

Create a .csv. Each columns contains the asset to NOT consider when training
a wide model for a given product_ID

"""


import dask
import dask.dataframe as dd
import pandas as pd

data = dd.read_csv("preprocess/training_wide_mean_ret_interpolated_vol/*",sample=10**9).compute()

def listing(file):

    liste_assets = pd.DataFrame()

    for prod_id in range(1,10):
        liste_temp = 318*[0]
        delete = 0
        for i in range (1,319):
            delete = 0
            if any(item == 0 for item in file[file[f"TARGET_{prod_id}_"]>0][f"TARGET_{i}_"]):
                delete = 1

            if delete == 1:
                liste_temp[i-1]=i
        
        liste_assets[f"{prod_id}"] = liste_temp


    return liste_assets

liste_assets_to_delete = listing(data)
liste_assets_to_delete.to_csv("liste_asset_to_delete.csv")
