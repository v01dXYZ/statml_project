# CFM Challenge


## How to

### Config file

* `cfm_credentials.toml` specify `username`/`password`. For example:

  ```toml
  username="rob.miles"
  password="p/q2-q4!a"
  ```

### Code

* `models.py` Contains the two base classes for model generation
  `LongFormatModel` and `WideFormatModel`.
* `submit.py` Code to request to the challenge server and get the test
  score.
* `main.py` Main file to add specific models.
* `preprocess.py` Script to generate the preprocesses datasets.
* `launch.py` Script to easily train and submit the models

### Data

* `data/` Raw data (the zipped files need to be uncompressed there
  too)
* `preprocess/` Preprocessed data
* `models/` MOJO models (beware training will write over the existing
  models with same name)
* `predictions/` Submitted predictions (timestamped)

## Add a specific model

You have to specify:

* `name` the name of the model used for storing the MOJO file and the
  predictions
* `train_data_frame`/`test_data_name` name of the train/test dataset
  to use
* `create_model` method that will generate the model (no input if
  `LongFormatModel`, takes `product_id` as  first parameter if
  `WideFormatModel`)
