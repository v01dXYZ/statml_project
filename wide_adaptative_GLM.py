import h2o
import pandas as pd
from TOOLS import mape
from TOOLS import Get_best_model_wide
from TOOLS import Search_best_model_lambda_wide
from h2o.estimators.glm import H2OGeneralizedLinearEstimator
import sys

d1 = int(sys.argv[1])
d2 = int(sys.argv[2])
    

liste_delete = pd.read_csv("liste_asset_to_delete.csv")

h2o.init()

data = h2o.import_file("preprocess/training_wide_mean_ret_interpolated_vol", pattern=".*.part")

features_keywords = ["mean","volatility"]
features_basic = [col for col in data.columns if any(item in col for item in features_keywords)]

for prod_id in range(d1,d2+1):

    features_delete = [features for features in features_basic if any("_"+str(item)+"_" in features for item in liste_delete[f"{prod_id}"])]
    features = [item for item in features_basic if item not in features_delete]

    data_by_asset = data[data[f"TARGET_{prod_id}_"]>0]
    
    train, valid = data_by_asset.split_frame(ratios=[0.8], seed = 42)

    liste_models = []
   
    for alpha in [0, 0.25, 0.50, 0.75, 1]:
       
        model = H2OGeneralizedLinearEstimator(
        alpha=alpha,
        lambda_search = True,
        family="gaussian")
       
        model.train(features,
                   f"TARGET_{prod_id}_",
                   training_frame = train,
                   validation_frame = valid,
                   weights_column = f"WEIGHT_GLM_{prod_id}_")
        
        liste_models.append(Search_best_model_lambda_wide(model, train, valid, prod_id))
       

    best_model = Get_best_model_wide(liste_models, valid, prod_id)
    best_model.model_id=f"wide_adaptative_GLM_{prod_id}"
    h2o.save_model(best_model, "generated_models/wide_asset_by_asset", force=True)
    test1 = best_model.predict(valid)
    test2 = max(0,best_model.predict(valid))
    test = test1-test2
    print(test)

    score = mape(max(0,best_model.predict(valid)),valid[f"TARGET_{prod_id}_"])
    
    file = open(f"wide_adaptative_score_{d1}_{d2}.csv",'a')
    file.write("Produit ")
    file.write(str(prod_id))
    file.write(" score obtenue sur le jeu valid ")
    file.write(str(score))
    file.write("\n")
    file.close() 
